const colors = {
  desertStorm: '#f7f7f5',
  white: '#fff',
  black: '#000',
};
export {colors};
