import React from 'react';
import {View, StyleSheet, Text} from 'react-native';
import Header from './component/Header';
import {scale, verticalScale, s} from 'react-native-size-matters';
import TopTab from '../topTab/TopTab';
import Footer from './component/Footer';
const HotelPage = () => {
  return (
    <View style={styles.container}>
      <Footer />
      <Header />
      <View style={styles.topTabStyle} />
      <View style={{height: '100%'}}>
        <TopTab />
      </View>
    </View>
  );
};

export default HotelPage;
const styles = StyleSheet.create({
  container: {flex: 1, backgroundColor: '#000', marginTop: verticalScale(20)},
  topTabStyle: {
    backgroundColor: '#fc9373',
    width: '100%',
    height: '15%',
    borderTopLeftRadius: scale(27),
    top: verticalScale(50),
    borderTopRightRadius: scale(27),
    position: 'absolute',
  },
});
