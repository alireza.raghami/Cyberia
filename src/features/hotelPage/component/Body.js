import React from 'react';
import {View, Image, StyleSheet, Text, FlatList} from 'react-native';
import {scale, verticalScale, s} from 'react-native-size-matters';
import {CheckData} from './CheckData';
import {colors} from '../../../utils/constants/theme';
const Body = () => {
  return (
    <View style={styles.main}>
      <View style={styles.container}>
        <View style={styles.Boxtitle}>
          <Text style={styles.textTitle}>Your Room</Text>
          <View style={styles.line} />
        </View>
        <View style={{alignItems: 'center'}}>
          <FlatList
            keyExtractor={item => item.key}
            horizontal
            data={CheckData}
            renderItem={({item, index}) => {
              return (
                <View style={styles.checkBoxcontainer}>
                  <View style={styles.checkBox}>
                    <Text style={styles.titleFlatList}>{item.title}</Text>
                    <Text style={styles.time}>{item.time}</Text>
                  </View>
                </View>
              );
            }}
          />
        </View>
        <View style={styles.roomBox}>
          <View style={{width: '90%', alignSelf: 'center'}}>
            <View style={styles.imageBox}>
              <Image
                style={styles.roomImage}
                source={{
                  uri: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTEkbTNRrQ1GYhDKy9tT5ZnwXfTOoynIPBx9Q&usqp=CAU',
                }}
              />
            </View>
            <Text style={styles.roomName}>King Room</Text>
            <View style={styles.priceBox}>
              <Text style={{color: '#000'}}>Room Only</Text>
              <Text style={styles.price}>$ 935</Text>
            </View>
            <Text style={styles.rightText}>Non - Refundeble</Text>
            <View style={styles.bottomBox}>
              <View style={styles.cancelbottom}>
                <Text style={styles.cancelText}>Cancel</Text>
              </View>
              <View style={styles.changeRoom}>
                <Text style={styles.changeRoomText}>Change Room</Text>
              </View>
            </View>
          </View>
        </View>
      </View>
    </View>
  );
};

export default Body;
const styles = StyleSheet.create({
  main: {
    width: '100%',
    backgroundColor: colors.desertStorm,
    borderTopLeftRadius: scale(20),
    borderTopRightRadius: scale(20),
    borderBottomLeftRadius: scale(35),
    borderBottomRightRadius: scale(35),
    height: '85%',
    alignItems: 'center',
  },
  container: {
    width: '90%',
    height: '100%',
  },
  Boxtitle: {
    height: verticalScale(50),
    justifyContent: 'flex-end',
  },
  textTitle: {
    color: colors.black,
    fontSize: s(16),
    fontWeight: 'bold',
  },
  line: {
    width: scale(400),
    height: verticalScale(2),
    borderRadius: 3,
    backgroundColor: colors.black,
    position: 'absolute',
    bottom: verticalScale(-5),
  },
  checkBoxcontainer: {
    height: verticalScale(160),
    justifyContent: 'center',
  },
  checkBox: {
    width: scale(130),
    height: verticalScale(85),
    borderRadius: scale(10),
    backgroundColor: colors.white,
    borderColor: colors.desertStorm,
    borderWidth: 1,
    marginHorizontal: scale(9),
    alignItems: 'center',
    justifyContent: 'center',
  },
  titleFlatList: {
    fontWeight: 'bold',
  },
  roomBox: {
    backgroundColor: colors.white,
    width: '100%',
    height: '52%',
    borderRadius: scale(20),
  },
  imageBox: {
    alignItems: 'center',
    width: '100%',
    marginTop: verticalScale(15),
    height: verticalScale(100),
  },
  roomImage: {
    width: '100%',
    height: '100%',
    borderRadius: scale(10),
  },
  roomName: {
    color: colors.black,
    fontWeight: 'bold',
    marginTop: verticalScale(10),
  },
  priceBox: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: verticalScale(15),
    alignItems: 'center',
  },
  price: {
    fontWeight: 'bold',
    color: colors.black,
  },
  rightText: {textAlign: 'right', marginTop: verticalScale(3)},
  bottomBox: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: verticalScale(20),
  },
  cancelbottom: {
    borderWidth: 1,
    width: scale(130),
    borderRadius: scale(4),
    height: verticalScale(28),
    alignItems: 'center',
    justifyContent: 'center',
  },
  cancelText: {
    color: colors.black,
  },
  changeRoom: {
    borderWidth: 1,
    width: scale(130),
    borderRadius: scale(4),
    height: verticalScale(30),
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: colors.black,
  },
  changeRoomText: {
    color: colors.white,
  },
  time: {marginTop: verticalScale(5)},
});
