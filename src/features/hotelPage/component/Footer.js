import React from 'react';
import {View, StyleSheet, Text, Image} from 'react-native';
import {scale, verticalScale, s} from 'react-native-size-matters';
import {colors} from '../../../utils/constants/theme';
const Footer = () => {
  return (
    <View style={styles.container}>
      <View>
        <Text style={styles.price}>Estimated cost : $264</Text>
        <Text style={styles.room}>King Room (Room Only)</Text>
      </View>
      <View style={styles.bottom}>
        <Text style={styles.bottomText}>Flight</Text>
        <Image
          style={styles.arrow}
          source={require('../../../assets/hotelPage/arrow.png')}
        />
      </View>
    </View>
  );
};

export default Footer;
const styles = StyleSheet.create({
  container: {
    width: '100%',
    backgroundColor: 'gray',
    position: 'absolute',
    bottom: 1,
    height: verticalScale(90),
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  price: {
    marginTop: verticalScale(45),
    marginLeft: scale(30),
    fontWeight: 'bold',
    color: colors.black,
  },
  room: {
    // marginTop: verticalScale(70),
    marginLeft: scale(30),
    fontWeight: 'bold',
    color: colors.white,
  },
  bottom: {
    borderWidth: 1,
    backgroundColor: colors.black,
    width: scale(110),
    height: verticalScale(34),
    marginTop: verticalScale(50),
    marginRight: scale(30),
    borderRadius: scale(8),
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
  },
  bottomText: {
    color: colors.white,
    fontWeight: 'bold',
  },
  arrow: {
    width: scale(12),
    height: verticalScale(12),
    transform: [{rotate: '180deg'}],
    marginLeft: scale(10),
  },
});
