import React from 'react';
import {View, Image, StyleSheet, Text} from 'react-native';
import {scale, verticalScale, s} from 'react-native-size-matters';
import {colors} from '../../../utils/constants/theme';
const Header = () => {
  return (
    <View style={styles.container}>
      <View style={styles.left}>
        <Image
          source={require('../../../assets/hotelPage/back.png')}
          style={styles.backIcon}
        />
        <Text style={styles.textHeader}>Hotel</Text>
      </View>
      <Image
        source={require('../../../assets/hotelPage/more.png')}
        style={styles.moreIcon}
      />
    </View>
  );
};

export default Header;
const styles = StyleSheet.create({
  container: {
    width: '100%',
    backgroundColor: colors.black,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    height: verticalScale(50),
  },
  backIcon: {
    width: scale(20),
    height: verticalScale(27),
    marginLeft: scale(10),
  },
  moreIcon: {
    width: scale(15),
    height: verticalScale(20),
    marginRight: scale(10),
    resizeMode: 'contain',
  },
  left: {
    flexDirection: 'row',
    width: '22%',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  textHeader: {
    color: colors.white,
    fontSize: s(15),
    fontWeight: 'bold',
  },
});
