import React from 'react';
import {View, StyleSheet, Text} from 'react-native';
import {scale, verticalScale, s} from 'react-native-size-matters';
import {createMaterialTopTabNavigator} from '@react-navigation/material-top-tabs';
import {NavigationContainer} from '@react-navigation/native';
import Body from '../hotelPage/component/Body';
import AmentiesTab from './component/AmentiesTab';
import OverView from './component/OverView';
const Tab = createMaterialTopTabNavigator();

const navigationTheme = {
  colors: {
    background: 'transparent',
  },
};
const TopTab = () => {
  return (
    <View style={styles.container}>
      <NavigationContainer theme={navigationTheme}>
        <Tab.Navigator
          initialRouteName={'Room'}
          screenOptions={{
            tabBarIndicatorStyle: {
              opacity: 0,
            },
          }}>
          <Tab.Screen
            options={{
              tabBarLabel: ({focused, color, size}) => (
                <Text
                  style={{
                    color: focused ? '#000' : color,
                    fontWeight: focused ? 'bold' : 'normal',
                    position: 'absolute',
                    top: verticalScale(-10),
                    fontSize: s(15),
                    right: scale(-20),
                  }}>
                  OverView
                </Text>
              ),
            }}
            name="OverView"
            component={OverView}
          />
          <Tab.Screen
            options={{
              tabBarLabel: ({focused, color, size}) => (
                <Text
                  style={{
                    color: focused ? '#000' : color,
                    fontWeight: focused ? 'bold' : 'normal',
                    position: 'absolute',
                    top: verticalScale(-10),
                    fontSize: s(15),
                    right: scale(-31),
                  }}>
                  Amenties
                </Text>
              ),
            }}
            name="Amenties"
            component={AmentiesTab}
          />
          <Tab.Screen
            options={{
              tabBarLabel: ({focused, color, size}) => (
                <Text
                  style={{
                    color: focused ? '#000' : color,
                    fontWeight: focused ? 'bold' : 'normal',
                    position: 'absolute',
                    top: verticalScale(-10),
                    fontSize: s(15),
                  }}>
                  Room
                </Text>
              ),
            }}
            name="Room"
            component={Body}
          />
        </Tab.Navigator>
      </NavigationContainer>
    </View>
  );
};

export default TopTab;
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
