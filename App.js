import React from 'react';
import {View, StatusBar} from 'react-native';
import HotelPage from './src/features/hotelPage/HotelPage';
import SystemNavigationBar from 'react-native-system-navigation-bar';
SystemNavigationBar.navigationHide();
const App = () => {
  return (
    <View style={{flex: 1}}>
      <StatusBar backgroundColor={'#000'} />
      <HotelPage />
    </View>
  );
};

export default App;
